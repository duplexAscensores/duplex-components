
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const path = require('path');
const webpack = require('webpack');

const baseConfig = (configScope) => {
  return {
    mode: 'production',
    devtool: 'source-map',
    entry: {
      app: path.resolve('index.js')
    },
    output: {
      path: configScope.distPath,
      filename: '[name].js',
    },
    module: {
      rules: [
        {
          test: /\.js$/,
          exclude: /node_modules/,
          loader: 'babel-loader',
          options: {
            plugins: ['@babel/plugin-syntax-dynamic-import'],
            presets: [
              [
                '@babel/preset-env',
                {
                  useBuiltIns: 'usage',
                  targets: '>1%, not dead, not ie 11',
                  corejs: 3
                }
              ]
            ]
          }
        },
        {
          test: /\.css$/,
          use: ['style-loader', 'css-loader'],
        }
      ]
    },
    plugins: [
      new CleanWebpackPlugin(),
      new webpack.ProgressPlugin(),
    ]
  }
};

module.exports = {
  baseConfig
};
