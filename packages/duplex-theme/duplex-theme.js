import { fonts } from './src/duplex-theme-font-family';
import { themeColor } from './src/duplex-theme-color';
import { fontSize } from './src/duplex-theme-text-size';
import { normalize } from './src/duplex-theme-normalize';
import { themeClass } from './src/duplex-theme-class-helpers';

const shareStyle = [
  normalize,
  fonts,
  themeColor,
  fontSize,
  themeClass
];

export { shareStyle };
