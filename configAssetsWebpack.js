
const path = require('path');
const webcomponents = path.resolve(__dirname, 'node_modules', '@webcomponents', 'webcomponentsjs');

const copyAssets = (assets = []) => {
  return [
    {
      from: path.resolve(`${webcomponents}/webcomponents-loader.js`),
      to: 'vendor',
    },
    {
      from: path.resolve(`${webcomponents}/webcomponents-bundle.js`),
      to: 'vendor',
    },
    {
      from: path.resolve(`${webcomponents}/custom-elements-es5-adapter.js`),
      to: 'vendor',
    },
    ...assets,
  ];
};

module.exports = {
  copyAssets
};
